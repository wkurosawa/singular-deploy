require 'mina/git'

set :domain, 'wildfly.929.im'
set :deploy_to, '/var/www/singular_resources'
set :repository, 'git@github.com:flama/resources.git'
set :branch, 'master'

set :shared_paths, []

set :user, 'root'
set :forward_agent, true
set :term_mode, nil

task :environment do
end

task :setup => :environment do
end

desc "Deploys the current version to the server."
task :deploy => :environment do
  to :before_hook do
    # Put things to run locally before ssh
  end
  deploy do
    invoke :'git:clone'
    invoke :'deploy:link_shared_paths'
    queue "cp -LR /var/www/singular_resources/current/* /var/www/wildfly/standalone/deployments/singular-static.war/resources"
    invoke :'deploy:cleanup'
  end
end
